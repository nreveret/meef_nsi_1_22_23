def triInsertion(A: list) -> None:
    """
    Trie la liste A par insertion
    Le tri est en place
    """
    for i in range(1, len(A)):
        elt = A[i]
        j = i
        while j > 0 and A[j-1] > elt:
            A[j] = A[j-1]
            j -= 1
        A[j] = elt


def triFusion(T: list) -> list:
    """
    Partage le tableau en deux sous-tableaux, trie chacun d'eux et renvoie leur fusion
    T est le tableau à trier
    Renvoie un nouveau tableau contenant les mêmes valeurs que T mais triées
    """
    # Cas de base
    if len(T) <= 1:
        return T
    else:
        # L'indice du "milieu" du tableau
        milieu = len(T) // 2

        # Les sous-tableaux de gauche et de droite
        gauche = []
        droite = []
        # On copie la première moitié de T dans gauche,
        # La seconde dans droite
        for i in range(len(T)):
            if i < milieu:
                gauche.append(T[i])
            else:
                droite.append(T[i])

        gauche = triFusion(gauche)
        droite = triFusion(droite)

        # On renvoie leur fusion
        return fusion(gauche, droite)


def fusion(G: list, D: list) -> list:
    """
    Fusionne les deux tableaux proposés en un nouveau tableau
    A chaque étape on sélectionne le plus petit élément situé au 
    début de l'un des deux tableaux
    G et D sont les tableaux à fusionner
    Renvoie un nouveau tableau contenant les valeurs de G et D
    """

    R = []  # Le tableau qui sera renvoyé

    # On ajoute "infini" à la fin des tableaux pour simplifier la fusion
    G.append(float('inf'))
    D.append(float('inf'))

    while len(G) > 1 or len(D) > 1:  # les seuls éléments restants seront les infinis
        if G[0] <= D[0]:
            R.append(G.pop(0))
        else:
            R.append(D.pop(0))

    return R


def comparaisons(t_min, t_max, pas, echantillon = 100) -> None:
    from random import shuffle
    from statistics import mean
    from time import perf_counter_ns
    import matplotlib.pyplot as plt

    tailles = [taille for taille in range(t_min, t_max+1, pas)]
    durees_insertion = []
    durees_fusion = []

    resultats_insertion = {}
    resultats_fusion = {}


    for i in range(echantillon) :
        resultats_insertion[i] = []
        resultats_fusion[i] = []
        for taille in range(t_min, t_max+1, pas) :
            A_insertion = [k for k in range(taille)]
            shuffle(A_insertion)
            A_fusion = A_insertion[::-1]
            debut = perf_counter_ns()
            triInsertion(A_insertion)
            fin = perf_counter_ns()
            resultats_insertion[i].append(fin-debut)
            debut = perf_counter_ns()
            triFusion(A_fusion)
            fin = perf_counter_ns()
            resultats_fusion[i].append(fin-debut)
    
    for t in range(len(tailles)) :
        durees_insertion.append(mean([resultats_insertion[e][t] for e in range(echantillon)]))
        durees_fusion.append(mean([resultats_fusion[e][t] for e in range(echantillon)]))
    
    plt.plot(tailles, durees_insertion, label="Tri par insertion")
    plt.plot(tailles, durees_fusion, label = "Tri fusion")
    plt.xlabel("Taille du tableau")
    plt.ylabel("Temps (ns)")
    plt.title("Comparaison des tri par insertion et fusion")
    plt.tight_layout()
    plt.legend()
    plt.show()

if __name__ == "__main__":
    # from random import shuffle
    # N = 10
    # A = [k for k in range(N)]
    # shuffle(A)
    # print(triFusion(A))

    comparaisons(1, 2000, 100, 10)